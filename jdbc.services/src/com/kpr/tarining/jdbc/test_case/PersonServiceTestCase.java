/*
Requirement:
    To test the conditions in the PersonService.
    
Entity:
    1.PersonServiceTestCase
    2.AddressService
    3.AppExcetion
    4.Person
    5.Address
    
Function declaration:
    public void personCreationTest() {}
    public void personCreationTest1() {}
    public void personCreationTest2() throws AppException {}
    public void personReadTest() {}
    public void personReadTest1() {}
    public void personReadTest2() {}
    public void personReadAllTest() {}
    public void personUpdationTest() {}
    public void personUpdationTest1() {}
    public void personDeleteTest() {}
    public void personDeleteTest1() {}
    
Jobs to be done:
    1. Check whether the create throws AppException for postal_code is zero.
        1.1 Set the values for the person
        1.2 Set the address for the person with postal_code as zero.
        1.3 Check whether the create in PersonService throws AppException for the given values as the postal_code is zero.
    2. Check whether the create throws exception for invalid emailid with correct postal_code.
        2.1 Set the values for the person with invalid emailid.
        2.2 Set the address for the person with correct postal_code.
        2.3 Check whether the create in PersonService throws AppException for the given values as the emailid is invalid.
    3. Check whether the address creation is done for the person having valid emailid.
        3.1 Set the values for the person.
        3.2 Set the address values for the person.
        3.3 Create the person and store the Long value in id.
        3.4 Check whether the id created is greater than zero to confirm person creation.
    4. Check whether the read query throws AppException with invalid id.
        4.1 Read the person with invalid id and to return the person's address.
        4.2 Check whether it throws AppException as the id is invalid.
    5. Check whether the read query executes for the valid id.
        5.1 Read the person with valid id and to return the person's address.
        5.1 Check whether the returned value of the person is not null.
    6. Check whether the read query executes with valid id and no need of person's address.
        6.1 Read the person with valid id and not to return the person's address.
        6.2 Check whether the returned value of the person is not null.
    7. Check whether the readAll query executes for the person.
        7.1 Read all the person with thier address.
        7.2 Check whether the returned value is not null.
    8. Check whether the update query executes for valid id and emailid with correct postal_code.
        8.1 Set the values for the person.
        8.2 Set the values for the person's address.
        8.3 Check whether the returned value is equal to the expected value.
    9. Check whether the update query throws AppException with postal_code as zero.
        9.1 Set the values for the person.
        9.2 Set the values for the person's address with postal_code as zero.
        9.3 Check whether it throws AppException as the postal_code in the given value as zero.
    10. Check whether the delete query executes for the valid id.
        10.1 Delete the valid person id's record.
        10.2 Check whether it returned value is equal to the expected value.
    11. Check whether the delete query throws AppException for the invalid id.
        11.1 Delete the invalid peron id's record.
        11.2 Check whether it throws AppException as the id is invalid.
        
Pseudo code:
class PersonServivceTestCase {
    
    @Test(priority = 1, description = "Address Creation with postal_code as 0", expectedExceptions = AppException.class)
    public void personCreationTest() {
        Person person = new Person("name", "emailid", Date.valueOf("date"));
        Address address = new Address("street", "city", 0);
        PersonService.create(person, address);
    }
    
    @Test(priority = 2, description = "Address Creation with postal_code not as 0 and invalid emailid", expectedExceptions = AppException.class)
    public void personCreationTest1() {
        Person person = new Person("name", "emailid", Date.valueOf("date"));
        Address address = new Address("street", "city", postal_code);
        PersonService.create(person, address);
    }
    
    @Test(priority = 3, description = "Emailid is valid")
    public void personCreationTest2() throws AppException {
        Person person = new Person("name", "emailid", Date.valueOf("date"));
        Address address = new Address("street", "city", postal_code);
        Long id = PersonService.create(person, address);
        Assert.assertTrue(id > 0);        
    }
    
    @Test(priority = 4, description = "invalid id", expectedExceptions = AppException.class)
    public void personReadTest() {
        PersonService.read(0, true);
    }   
    
    @Test(priority = 5, description = "Valid id and the boolean flag is true")
    public void personReadTest1() {
        
        Assert.assertTrue(PersonService.read(1, true) != null);
    }
    @Test(priority = 6, description = "Valid id and the boolean flag is false")
    public void personReadTest2() {
        
        Assert.assertTrue(PersonService.read(1, false) != null);
    }
    @Test(priority = 7, description = "Reading all persons with address")
    public void personReadAllTest() {
        
        Assert.assertTrue(AddressService.readAll() != null);
    }
    @Test(priority = 8, description = "In Person, Address Updation with postal_code not as 0 and email id is unique and valid")
    public void personUpdationTest() {
        Person person =  new Person("name", "emailid", Date.valueOf("date"));
        Address address = new Address("street", "city", postal_code);
        Assert.assertEquals(PersonService.update(4,person, address), "Updation Successful");
    }
    
    @Test(priority = 9, description = "In Person, Address Updation with postal_code as 0", expectedExceptions = AppException.class)
    public void personUpdationTest1() {
        Person person =  new Person("name", "emailid", Date.valueOf("date"));
        Address address = new Address("street", "city", 0);
        PersonService.update(20, person, address);
    }
    
    @Test(priority = 10, description = "Deleting person with valid id")
    public void personDeleteTest() {
        
        Assert.assertEquals(PersonService.delete(11), "Deletion Successful");
    }
    
    @Test(priority = 11, description = "Deleting address with invalid id", expectedExceptions = AppException.class)
    public void personDeleteTest1() {
        PersonService.delete(100);
    }
}

*/

package com.kpr.tarining.jdbc.test_case;

import java.sql.Date;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.exceptions.AppException;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.service.AddressService;
import com.kpr.training.jdbc.service.PersonService;

public class PersonServiceTestCase {
    
    @Test(priority = 1, description = "Address Creation with postal_code as 0", expectedExceptions = AppException.class)
    public void personCreationTest() {
        Person person = new Person("Bharathwaj", "bharathwaj549@gmail.com", Date.valueOf("2001-01-20"));
        Address address = new Address("NGR Road", "Coimbatore", 0);
        PersonService.create(person, address);
    }
    
    @Test(priority = 2, description = "Address Creation with postal_code not as 0 and invalid emailid", expectedExceptions = AppException.class)
    public void personCreationTest1() {
        Person person = new Person("Bharathwaj", "bharathwaj549gmail.com", Date.valueOf("2001-01-11"));
        Address address = new Address("NGR Road", "Tiruppur", 641605);
        PersonService.create(person, address);
    }
    
    @Test(priority = 3, description = "Emailid is valid")
    public void personCreationTest2() throws AppException {
        Person person = new Person("Dhivya", "div@gmail.com", Date.valueOf("2001-01-11"));
        Address address = new Address("KPR Road", "Tiruppur", 641607);
        Long id = PersonService.create(person, address);
        Assert.assertTrue(id > 0);        
    }
    
    @Test(priority = 4, description = "invalid id", expectedExceptions = AppException.class)
    public void personReadTest() {
    	PersonService.read(0, true);
    }	
    
    @Test(priority = 5, description = "Valid id and the boolean flag is true")
    public void personReadTest1() {
    	
    	Assert.assertTrue(PersonService.read(2, true) != null);
    }
    @Test(priority = 6, description = "Valid id and the boolean flag is false")
    public void personReadTest2() {
    	
    	Assert.assertTrue(PersonService.read(1, false) != null);
    }
    @Test(priority = 7, description = "Reading all persons with address")
    public void personReadAllTest() {
        
        Assert.assertTrue(AddressService.readAll() != null);
    }
    @Test(priority = 8, description = "In Person, Address Updation with pincode not as 0 and email id is unique and valid")
    public void personUpdationTest() {
    	Person person =  new Person("Krithic", "kiru@gmail.com", Date.valueOf("2020-10-30"));
        Address address = new Address("MG Road", "Bangalore", 628402);
        Assert.assertEquals(PersonService.update(4,person, address), "Updation Successful");
    }
    
    @Test(priority = 9, description = "In Person, Address Updation with pincode as 0", expectedExceptions = AppException.class)
    public void personUpdationTest1() {
    	Person person =  new Person("Balaji", "killerking@gmail.com", Date.valueOf("2020-10-30"));
        Address address = new Address("St Michael's Road", "Bangalore", 0);
        PersonService.update(20, person, address);
    }
    
    @Test(priority = 10, description = "Deleting person with valid id")
    public void personDeleteTest() {
        
        Assert.assertEquals(PersonService.delete(1), "Deletion Successful");
    }
    
    @Test(priority = 11, description = "Deleting address with invalid id", expectedExceptions = AppException.class)
    public void personDeleteTest1() {
        PersonService.delete(100);
    }
}
