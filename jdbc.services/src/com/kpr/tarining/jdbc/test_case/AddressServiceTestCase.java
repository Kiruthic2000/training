/*
Requirement:
    To test the conditions in the AddressService.
    
Entity:
    1.AddressServiceTestCase
    2.AddressService
    3.AppException
    4.Address
    
Function declaration:
    public void addressCreationTest() throws AppException {}
    public void addressCreationTest1() throws AppException {}
    public void addressReadTest() throws AppException {}
    public void addressReadTest1() throws AppException {}
    public void addressReadAllTest() throws AppException {}
    public void addressUpdationTest() throws AppException {}
    public void addressUpdationTest1() throws AppException {}
    public void addressUpdateTest2() throws AppException {}
    public void addressUpdateTest3() throws AppException {}
    public void addressDeleteTest() throws AppException {}
    public void addressDeleteTest1() throws AppException {}
    
Jobs to be done:
    1. Check if the pincode is not as zero.
        1.1 Set the values of the address.
        1.2 Create the given address and store the id.
        1.3 Check whether the id is greater than zero.
    2. Check what if the pincode is zero and it throws AppException.
        2.1 Set the address value with pincode as zero.
        2.2 Create the given address. 
    3. Check whether the query reads the valid id.
        3.1 Check whether the given id values are not null.
    4. Check whether the query throws exception for the invalid id to read.
        4.1 Check whether the given invalid id throws AppException to read.
    5. Check whether the query reads all the addresses in the table.
        5.1 Check whether it doesn't returns null.
    6. Check whether the upadtion of new address with pincode not as zero executes.
        6.1 Set the values of address to be updated.
        6.2 Check if it returns the expected output.
    7. Check whether the updation of address with pincode as zero executes.
        7.1 Set the values of address with pincode as zero.
        7.2 Check whether it throws the AppException as the pincode is zero.
    8. Check whether the updation executes with valid id.
        8.1 Set the address values with existing id.
        8.2 Update the id with given address values.
    9. Check whether the updation executes with invalid id.
        9.1 Set the address values.
        9.2 Check whether it throws the AppException as the id is invalid.
    10. Check whether the deletion executes for the valid id.
        10.1 Check whether the deletion method returns the expected output.
    11. Check whether the deletion executes with invalid id.
        11.1 Check whether it throws AppException as id not exist or invalid.
        
Pseudo code:

class AddressServiceTestCase {

    @Test(priority = 1, description = "Address Creation with pincode not as 0")
    public void addressCreationTest() throws AppException {
        Address address = new Address("street", "city", pincode);
        long id = AddressService.create(address);
        Assert.assertTrue(id > 0);
    }
    
    @Test(priority = 2, description = "Address Creation with pincode as 0", expectedExceptions = AppException.class)
    public void addressCreationTest1() throws AppException {
        Address address = new Address("street", "city", 0);
        AddressService.create(address);
    }
    
    @Test(priority = 3, description = "Reading address with valid id")
    public void addressReadTest() throws AppException {
        
        Assert.assertTrue(AddressService.read(id) != null);
    }
    
    @Test(priority = 4, description = "Reading address with invalid id", expectedExceptions = AppException.class)
    public void addressReadTest1() throws AppException {
        AddressService.read(id);
    }
    
    @Test(priority = 5, description = "Reading all address")
    public void addressReadAllTest() throws AppException {
        
        Assert.assertTrue(AddressService.readAll() != null);
    }
    
    @Test(priority = 6, description = "Address Updation with pincode not as 0")
    public void addressUpdationTest() throws AppException {
        Address address = new Address("street", "city", pincode);
        Assert.assertEquals(AddressService.update(id, address), "Updation Successful");
    }
    
    @Test(priority = 7, description = "Address Updation with pincode as 0", expectedExceptions = AppException.class)
    public void addressUpdationTest1() throws AppException {
        Address address = new Address("street", "city", 0);
        AddressService.update(id, address);
    }
    
    @Test(priority = 8, description = "Updating address with valid id")
    public void addressUpdateTest2() throws AppException {
        Address address = new Address("street", "city", pincode);
        AddressService.update(id, address);
    }
    
    @Test(priority = 9, description = "Updating address with invalid id", expectedExceptions = AppException.class)
    public void addressUpdateTest3() throws AppException {
        Address address = new Address("street", "city", pincode);
        AddressService.update(100, address);
    }
    
    @Test(priority = 10, description = "Deleting address with valid id")
    public void addressDeleteTest() throws AppException {
        
        Assert.assertEquals(AddressService.delete(id), "Deletion Successful");
    }
    
    @Test(priority = 11, description = "Deleting address with invalid id", expectedExceptions = AppException.class)
    public void addressDeleteTest1() throws AppException {
        AddressService.delete(id);
    }
    
}
*/
package com.kpr.tarining.jdbc.test_case;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.kpr.training.jdbc.exceptions.AppException;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.service.AddressService;

public class AddressServiceTestCase {

    @Test(priority = 1, description = "Address Creation with pincode not as 0")
    public void addressCreationTest() throws AppException {
        Address address = new Address("MG Road", "Bangalore", 628402);
        long id = AddressService.create(address);
        Assert.assertTrue(id > 0);
    }
    
    @Test(priority = 2, description = "Address Creation with pincode as 0", expectedExceptions = AppException.class)
    public void addressCreationTest1() throws AppException {
        Address address = new Address("NGR Road", "Tiruppur", 0);
        AddressService.create(address);
    }
    
    @Test(priority = 3, description = "Reading address with valid id")
    public void addressReadTest() throws AppException {
        
        Assert.assertTrue(AddressService.read(12) != null);
    }
    
    @Test(priority = 4, description = "Reading address with invalid id", expectedExceptions = AppException.class)
    public void addressReadTest1() throws AppException {
        AddressService.read(100);
    }
    
    @Test(priority = 5, description = "Reading all address")
    public void addressReadAllTest() throws AppException {
        
        Assert.assertTrue(AddressService.readAll() != null);
    }
    
    @Test(priority = 6, description = "Address Updation with pincode not as 0")
    public void addressUpdationTest() throws AppException {
        Address address = new Address("MG Road", "Bangalore", 628402);
        Assert.assertEquals(AddressService.update(13, address), "Updation Successfull");
    }
    
    @Test(priority = 7, description = "Address Updation with pincode as 0", expectedExceptions = AppException.class)
    public void addressUpdationTest1() throws AppException {
        Address address = new Address("MG Road", "Bangalore", 0);
        AddressService.update(20, address);
    }
    
    @Test(priority = 8, description = "Updating address with valid id")
    public void addressUpdateTest2() throws AppException {
        Address address = new Address("942,f/6, SVL Nagar, Manthithoppu", "Kovilpatti", 628502);
        AddressService.update(10, address);
    }
    
    @Test(priority = 9, description = "Updating address with invalid id", expectedExceptions = AppException.class)
    public void addressUpdateTest3() throws AppException {
        Address address = new Address("942,f/6, SVL Nagar, Manthithoppu", "Kovilpatti", 628502);
        AddressService.update(100, address);
    }
    
    @Test(priority = 10, description = "Deleting address with valid id")
    public void addressDeleteTest() throws AppException {
        
        Assert.assertEquals(AddressService.delete(28), "Deletion Successfull");
    }
    
    @Test(priority = 11, description = "Deleting address with invalid id", expectedExceptions = AppException.class)
    public void addressDeleteTest1() throws AppException {
        AddressService.delete(100);
    }
    
}
