package com.kpr.training.jdbc.constant;

public class QueryStatements {

	public static final String createAddressQuery =
			"INSERT INTO `jdbc.services`.address (street, city, postal_code) VALUES (?, ?, ?)";
	public static final String emailUnique =
			"SELECT person.id FROM `jdbc.services`.person WHERE person.email = ?";
	public static final String readAddressQuery =
			"SELECT address.id, address.street, address.city, address.postal_code FROM  `jdbc.services`.address WHERE `id` = ?";

	public static final String readAllAddressQuery =
			"SELECT address.id, address.street, address.city, address.postal_code FROM  `jdbc.services`.address";

	public static final String updateAddressQuery =
			"UPDATE `jdbc.services`.address SET street = ?, city = ?, postal_code = ? WHERE id = ?";

	public static final String deleteAddressQuery =
			"DELETE FROM  `jdbc.services`.address WHERE id=?";

	public static final String createPersonQuery =
			"INSERT INTO `jdbc.services`.person (name, email, birth_date, address_id) VALUES (?, ?, ?, ?)";

	public static final String readPersonQuery =
			"SELECT person.id, person.name, person.email, person.address_id, person.birth_date, person.created_date FROM  `jdbc.services`.person WHERE `id` = ?";

	public static final String readAllPersonQuery =
			"SELECT person.id, person.name, person.email, person.address_id, person.birth_date, person.created_date FROM  `jdbc.services`.person";

	public static final String updatePersonQuery =
			"UPDATE `jdbc.services`.person LEFT JOIN `jdbc.services`.address ON person.address_id = address.id SET address.street = ?, address.city = ?, address.postal_code = ?, person.name = ?, person.email = ?, person.birth_date = ? WHERE person.id = ?";

	public static final String deletePersonQuery =
			"DELETE `jdbc.services`.address, `jdbc.services`.person FROM `jdbc.services`.person INNER JOIN `jdbc.services`.address ON person.address_id = address.id WHERE person.id = ?";
}
