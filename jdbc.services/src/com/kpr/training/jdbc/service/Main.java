package com.kpr.training.jdbc.service;


import java.sql.Date;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.model.Person;

public class Main {
	public static void main(String[] args) {
		Person person = new Person("Kiruthic P", "kiruthic2109@gmail.com",Date.valueOf("2000-09-21"));
		Address address = new Address("Karuparayan kovil street", "Tirupur", 641652);
		PersonService.create(person, address);
		PersonService.readAll();
	}
}
