/*
Requirement:
    To update a record for a particular id in address.
    
Entity:
    AddressServiceUpdate
    
Function declaration:
    public static void update(long id, Address address)
    
Jobs to be done:
    1. Create an instance of the class JdbcConnection as jc.
    2. Establish the connection using jdbc driver from createConnection method.
    3. Prepare an update query and store in query as type of String.
    4. Create a prepareStatement of the query using jc.con as jc.ps.
    5. Assign the values of the address table
        5.1 Mention the id to which the values to be updated.
    6. Execute jc.ps
    7. Close the connection established.
Pseudo code:
class AddressServiceUpdate {

    public static void update(long id, Address address) {
        JdbcConnection jc = new JdbcConnection();
        jc.createConnection();
        String query = "UPDATE `jdbc`.address SET street = ?, city = ?, postal_code = ? WHERE id = ?";
        try {
            jc.ps = jc.con.prepareStatement(query);
            jc.ps.setString(1, address.getStreet());
            jc.ps.setString(2, address.getCity());
            jc.ps.setInt(3, address.getPostalCode());
            jc.ps.setLong(4, id);
            jc.ps.executeUpdate();
        } catch (SQLException e) {
            throw new AppException(ExceptionCode.SQLException);
        }
        jc.closeConnection();
    }

}

*/

package com.kpr.training.jdbc.service;

import java.sql.SQLException;
import com.kpr.training.jdbc.exceptions.AppException;
import com.kpr.training.jdbc.exceptions.ExceptionCode;
import com.kpr.training.jdbc.model.Address;

public class AddressServiceUpdate {

	public static void update(long id, Address address) {
		JdbcConnection jc = new JdbcConnection();
		jc.createConnection();
		String updateQuery =
				"UPDATE `jdbc.services`.address SET street = ?, city = ?, postal_code = ? WHERE id = ?";
		try {
			jc.ps = jc.con.prepareStatement(updateQuery);
			jc.ps.setString(1, address.getStreet());
			jc.ps.setString(2, address.getCity());
			jc.ps.setInt(3, address.getPostalCode());
			jc.ps.setLong(4, id);
			jc.ps.executeUpdate();
		} catch (SQLException e) {
			throw new AppException(ExceptionCode.SQLException);
		}
		jc.closeConnection();
	}

}
