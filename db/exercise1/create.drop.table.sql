
CREATE TABLE training.doctor (doctor_id INT NOT NULL
  ,name VARCHAR(45) NOT NULL
  ,age INT NOT NULL
  ,specialization VARCHAR(45) NOT NULL
  ,experience INT NOT NULL
  ,dept_id INT NULL
  ,PRIMARY KEY (`doctor_id`));
      DESC training.doctor;
DROP TABLE training.doctor;
