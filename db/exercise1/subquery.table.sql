CREATE SCHEMA training;
 CREATE TABLE training.department( dept_id INT NOT NULL
    ,department_name VARCHAR(45) NOT NULL
    ,PRIMARY KEY(dept_id));
INSERT INTO training.department (dept_id, department_name)
     VALUES ('1', 'CARDIOLOGY');
INSERT INTO training.department (dept_id, department_name)
     VALUES ('2', 'NEUROLOGY');
INSERT INTO training.department (dept_id, department_name)
     VALUES ('3', 'DERMATOLOGY');
INSERT INTO training.department (dept_id, department_name)
     VALUES ('4', 'OPTHALMOLOGY');
INSERT INTO training.department (dept_id, department_name)
     VALUES ('5', 'ANESTHESIOLOGY');
INSERT INTO training.department (dept_id, department_name)
     VALUES ('6', 'ONCOLOGY');
 CREATE TABLE training.doctor (doctor_id INT NOT NULL
    ,name VARCHAR(45) NOT NULL
    ,age INT NOT NULL
    ,specialization VARCHAR(45) NOT NULL
    ,experience INT NOT NULL
    ,dept_id INT NULL
    ,city VARCHAR(45) NOT NULL
    ,salary INT NOT NULL
    ,PRIMARY KEY (doctor_id)
    ,INDEX dept_id_idx (dept_id ASC) VISIBLE
    ,CONSTRAINT dept_id
        FOREIGN KEY(dept_id)
        REFERENCES training.department(dept_id)
        ON DELETE RESTRICT
        ON UPDATE CASCADE);
INSERT INTO training.doctor (doctor_id, name, age, specialization, experience, dept_id, city, salary) 
     VALUES ('1','karthikeyan','30','Cardiologist','7', '1', 'coimbatore', '1000000');
INSERT INTO training.doctor (doctor_id, name, age, specialization, experience, dept_id, city, salary) 
     VALUES ('2', 'boobalan', '29', 'Neurologist', '6', '2' , 'tirppur', '400000');
INSERT INTO training.doctor (doctor_id, name, age, specialization, experience, dept_id, city, salary) 
     VALUES ('3', 'gokul', '45', 'Dermatologist', '23', '3', 'salem', '800000');
INSERT INTO training.doctor (doctor_id, name, age, specialization, experience, dept_id, city, salary) 
     VALUES ('4', 'kiruthic', '27', 'Opthalmologist', '4', '4', 'erode', '400000');
INSERT INTO training.doctor (doctor_id, name, age, specialization, experience, dept_id, city, salary) 
     VALUES ('5', 'keerthana', '26', 'Anesthesiologist', '3', '5', 'madurai', '500000');
INSERT INTO training.doctor (doctor_id, name, age, specialization, experience, dept_id, city, salary) 
     VALUES ('6', 'mani', '35', 'Oncologist', '13', '6', 'theni', '700000');
SELECT doctor_id
    ,name
    ,age
    ,department_name
    ,specialization
    ,experience
    ,city
    ,salary
  FROM training.doctor, training.department
 WHERE training.doctor.dept_id = training.department.dept_id;
SELECT name
    ,age
    ,specialization
    ,experience
    ,city
    ,salary
  FROM training.doctor
 WHERE  dept_id = (SELECT dept_id FROM training.department WHERE department_name = 'ONCOLOGY')