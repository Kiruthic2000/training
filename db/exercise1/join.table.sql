
     SELECT name
        ,age
        ,specialization
        ,experience
        ,city
        ,salary
       FROM training.doctor
 CROSS JOIN training.department;
     SELECT name
        ,age
        ,specialization
        ,experience
        ,city
        ,salary
       FROM training.doctor
 INNER JOIN training.department
         ON training.doctor.dept_id = training.department.dept_id;
      SELECT name
        ,age
        ,specialization
        ,experience
        ,city
        ,salary
       FROM training.doctor    
  LEFT JOIN training.department
         ON training.doctor.dept_id = training.department.dept_id;
     SELECT 
    name, age, specialization, experience, city, salary
FROM
    training.doctor
        RIGHT JOIN
    training.department ON training.doctor.dept_id = training.department.dept_id;     