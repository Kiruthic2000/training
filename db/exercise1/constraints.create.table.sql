
 CREATE TABLE training.doctor (doc_id INT NOT NULL
  ,name VARCHAR(45) NOT NULL
  ,age INT CHECK(age>22)
  ,specialization VARCHAR(45) NOT NULL
  ,experience INT NOT NULL
  ,dept_id INT NULL
  ,year_of_joining INT DEFAULT 2000
  ,PRIMARY KEY (`doc_id`)
  ,INDEX `depart_idx` (`dept_id` ASC) VISIBLE
  ,CONSTRAINT `depart`
    FOREIGN KEY (`dept_id`)
    REFERENCES `training`.`department` (`dept_id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE);
ALTER TABLE doctor
 ADD COLUMN dob DATE NOT NULL AFTER name ;
       DESC doctor;
ALTER TABLE doctor 
       DROP experience ;
       DESC doctor;