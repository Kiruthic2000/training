SELECT dept.department_number
      ,dept.department_name
      ,max(emp.annual_salary) AS highest_paid
      ,min(emp.annual_salary) AS lowest_paid
  FROM training.employee AS emp
      ,training.department AS dept
GROUP BY dept.department_number,dept.department_name