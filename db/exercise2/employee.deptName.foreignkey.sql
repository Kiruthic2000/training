SELECT emp.first_name
      ,emp.surname
      ,dept.department_name
  FROM training.employee emp
      ,training.department dept
 WHERE emp.department_number = dept.department_number