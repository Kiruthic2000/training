SELECT department_number
      ,department_name
  FROM training.department
 WHERE department_number
 NOT IN (SELECT department_number FROM training.employee)