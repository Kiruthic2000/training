SELECT first_name
      ,surname
  FROM training.employee
 WHERE MONTH(dob) = MONTH(NOW()) AND DAY(dob) = DAY(NOW())