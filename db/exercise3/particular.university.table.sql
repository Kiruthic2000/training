 SELECT u.university_name
  ,s.id AS roll_number
  ,s.name AS student_name
  ,s.gender
  ,s.dob
  ,s.address
  ,c.name
  ,d.dept_name
 FROM education.university u
  ,education.college c
  ,education.department d 
  ,education.college_department cd
  ,education.student s
 WHERE c.univ_code = u.univ_code 
   AND u.univ_code = d.univ_code 
   AND cd.college_id = c.id 
   AND cd.udept_code = d.dept_code
   AND s.college_id = c.id
   AND s.cdept_id = cd.cdept_id
   AND u.univ_code = '1'
   AND c.city ='coimbatore';