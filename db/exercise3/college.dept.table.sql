SELECT  college.code
       ,college.name AS college_name
       ,university.university_name 
       ,college.city
       ,college.state
       ,college.year_opened
       ,department.dept_name AS department_name
       ,employee.name AS hod_name
FROM education.college
    ,education.university
    ,education.department
    ,education.employee
    ,education.designation
	,education.college_department
WHERE college.univ_code = university.univ_code 
AND  department.univ_code = college.univ_code
AND  employee.college_id = college.id
AND  employee.cdept_id = college_department.cdept_id
AND  employee.desig_id = designation.id
AND  college_department.udept_code = department.dept_code
AND  college_department.college_id = college.id
AND designation.name = "hod"
AND department.dept_name = "cse" or "it"