SELECT de.name AS designation_name 
  ,de.rank 
  ,u.univ_code
  ,c.name AS college_name
  ,d.dept_name
  ,u.university_name
  ,c.city,c.state
  ,c.year_opened
 FROM education.university u
  ,education.college c
  ,education.department d 
  ,education.designation de
  ,education.college_department cd
  ,education.employee e 
 WHERE c.univ_code = u.univ_code 
   AND u.univ_code = d.univ_code 
   AND cd.college_id = c.id 
   AND cd.udept_code = d.dept_code
   AND e.college_id = c.id 
   AND e.cdept_id = cd.cdept_id 
   AND e.desig_id = de.id 
 ORDER BY de.rank;