package com.university.training.core;

import com.university.training.college.department;    //fully qualified name

public class Department {

    private String departmentName;

    public int departmentNumber;

    public Department() {
        //non-parameterized constructor
    }

    protected Department(int code, String name) {         //Parameterized constructor
        departmentNumber = code;
        departmentName = name;
    }
    {
        departmentName = "EEE";
        departmentNumber = 01;
    }
    public static void main(String[] args) {
        Department dept = new Department();
        System.out.println(" Department Name: " + dept.departmentName + " Department Number: " + dept.departmentNumber);   //prints "EEE"
        Department dept1 = new Department(02,"ECE");
        System.out.println(" Department Name: " + dept.departmentName + " Department Number: " + dept.departmentNumber);   //prints '1'
        System.out.println(" Department Name: " + dept1.departmentName + " Department Number: " + dept1.departmentNumber);   //prints "EEE"
    }
}