/*Requirements:
 * 		To write a program to print employees name list by implementing iterable interface.
 * 
 * Entity:
 * 		MyIterable, IterableInterface
 * 
 * Function Declaration:
 * 		public MyIterable(T[] t),
 * 		public Iterator<T> iterator().
 * 
 * Jobs To Be Done:
 * 		1)Importing the list, Arrays, Iterator.
 * 		2)Creating the MyIterable class that implements the Iterable.
 * 		3)Creating the local variable list as generic type.
 * 		4)Creating a method MyIterable of generic type  and assigning already defined String values from the main class.
 * 		5)Creating the main class as IterableInterface .
 * 		6)Declaring the String values in the name variable.
 * 		7)Creating the object myList for the variables to be iterated.
 * 		8)After assigning the variables in the list by iterator implement, the final result is printed.
 */

package generics;

import java.util.List;
import java.util.Arrays;
import java.util.Iterator;

class MyIterable<T> implements Iterable<T> {

	private List<T> list;

	public MyIterable(T[] t) {

		list = Arrays.asList(t);

	}


	public Iterator<T> iterator() {
		return list.iterator();
	}

}

public class IterableInterface {

	public static void main(String[] args) {

		String[] name = {"Balaji Pa", "Boobalan P", "Gokul D", "Karthikeyan C", "Kiruthic P"};
		MyIterable<String> myList = new MyIterable<>(name);

		for (String i : myList) {

			System.out.println(i);
		}
	}
}
