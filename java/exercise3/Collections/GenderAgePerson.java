/*Requirement:
 * 		To Write a program to filter the Person, who are male and age greater than 21
 * 
 * Entity:
 * 		GenderAgePerson
 * 
 * Function Declaration:
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1)In collection Package, The required packages are imported.
 * 		2)Creating the class GenderAgePerson
 * 		3)Accessing the precreated roster list in this class referred from Person.java
 * 		4)for each data in the list, checking the conditions if the person is male and greater than the age of 21
 * 		5) If true, the name is printed.
 */

package collections;

import java.util.List;

public class GenderAgePerson  {
	
    public static void main(String[] args) {
    	List<Person> roster = Person.createRoster();  
    	System.out.println("The Persons having Gender Male and Age greater than 21: ");
    	
    	for (Person p : roster) {
    	    if (p.getGender() == Person.Sex.MALE ) {
    	    	if(p.getAge() > 21) {
    	    		
    	    		System.out.println(p.getName());
    	    	}
    	    }
    	
	    }
    }
}