/*
 * Requirement:
 *      java program to demonstrate adding elements, displaying, removing, and iterating in hash set
 * Entity:
 *      HashSetExample
 * Function Declaration:
 *      public static void main(String[] args)
 * Jobs to be done:
 *      1)Create package named as collections.
 *      2)import HashSet package
 *      3)Create class HashSetExample and create main method in it.
 *      4)Create reference for HashSet and named as set.
 *      5)Adding elements ,removing element ,Displaying element and print it by using iterating method.  
 * */

package collections;

import java.util.HashSet;

public class HashsetExample {

    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();

        set.add(10);
        set.add(20);
        set.add(30);
        set.add(40);
        System.out.println("Displaying element in set " + set);
        set.remove(10);
        System.out.println("Printing the set after removing the elemnt " + set);
        System.out.println("Displaying element using iterating method ");
        for (int element : set) {
            System.out.println(element + " ");
        }
    }

}