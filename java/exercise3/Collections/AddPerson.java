/*
 * Requirement:
 *      List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
        - Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
        - Print the number of persons in roster List after the above addition.
        - Remove the all the person in the roster list
  Entity:
      AddPerson
  Function declaration:
      public static void main(String[] args) 
  Jobs to be done:
      1) Create package person and import IsoChronology,ArrayList,List and Stream.
      2) Create class Add person and create main method under it.
      3) Create reference for list named as roster and assign method createRoster for it.
      4) Create new ArrayList as newRoster.
      5) Add the given details in newRoster and add all newRoster elements in roster list.
      6) print all the value in the roster list.
      7) print the size of the list.
      8) Remove all the Elements from the list.
*/
package collections;

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class AddPerson {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(new Person("John", IsoChronology.INSTANCE.date(1980, 6, 20), Person.Sex.MALE,
                "john@example.com"));
        newRoster.add(new Person("Jade", IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(new Person("Donald", IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
        newRoster.add(new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12), Person.Sex.MALE,
                "bob@example.com"));
        roster.addAll(newRoster);
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
        System.out.println("Number of person in the roster list :" + roster.size());
        roster.clear();
        System.out.println("After removing all elements frfom the list :"+roster);
    }

}