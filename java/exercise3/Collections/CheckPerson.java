/*Requirement:
 * 		Consider the following Person:
 *          new Person(
 *              "Bob",
 *              IsoChronology.INSTANCE.date(2000, 9, 12),
 *              Person.Sex.MALE, "bob@example.com"));
 *     - Check if the above person is in the roster list obtained from Person class.
 * 
 * Entity:
 * 		CheckPerson
 * 
 * Function Declaration:
 * 		public static void main(String[] args)
 * 
 * Jobs To Be Done:
 * 		1) Import List and Stream and IsoChronology.
 *      2) Create class named as CheckPerson.
 *      3) Create a boolean present and initialize it with false.
 *      4) Create reference for list named as roster and assign method createRoster for it.
 *      5) Create a Person newPerson and assign the Person .
 *      6) Create reference for Stream named as stream and call the method stream.
 *      7) Using Stream forEach method and Using if conditions inside the forEach block and give the conditions to check whether the given person is present or not .
 *      8) Print whether person is present or not.
 */
package collections;

import java.time.chrono.IsoChronology;
import java.util.List;
import java.util.stream.Stream;

public class CheckPerson {

	public static boolean present = false;

	public static void main(String[] args) {
		List<Person> roster = Person.createRoster();
		Person newPerson = new Person("Bob", IsoChronology.INSTANCE.date(2000, 9, 12),
				Person.Sex.MALE, "bob@example.com");
		Stream<Person> stream = roster.stream();
		stream.forEach(person -> {
			if ((person.getName().equals(newPerson.getName()))
					&& (person.getBirthday().equals(newPerson.getBirthday()))
					&& (person.getEmailAddress().equals(newPerson.getEmailAddress()))
					&& (person.getGender().equals(newPerson.getGender()))) {
				present = true;
			}
		});
		if (present == true) {
			System.out.println("Person is present");
		} else {
			System.out.println("Person is not present");
		}

	}
}