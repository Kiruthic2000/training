/*
* Requirement:
*      sort the roster list based on the person's age in descending order using comparator
* Entity:
*      SortComparator
* Function declaration:
*      public static void main(String[] args)
* Jobs to be done:
*      1) Create package person and import Comparator,List and Stream from util package.
*      2) Create class SortComparator and Create main method under it.
*      3) Create reference for list named as roster and assign method createRoster for it.
*      4) By using comparator ,Compare the age of a person and sort it as ascending order.
*      5) Using stream print the person details.
*/
package collections;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class SortComparator {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        roster.sort(Comparator.comparing(Person::getAge));
        Stream<Person> stream = roster.stream();
        stream.forEach(person -> System.out.println(person.name + " " + person.birthday + " "
                + person.gender + " " + person.emailAddress));
    }
}