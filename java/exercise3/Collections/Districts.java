/*
 * Requirement:
 *      8 districts are shown below
 *  Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   to be converted to UPPERCASE.
 *Entity:
 *     Districts
 *Function Declaration:
 *     public static void main(String[] args)
 *Jobs to be done:
 *     1)Create package Collections and Import packages Arrays and List.
 *     2)Create class Districts and create main methid in it.
 *     3)Create reference for list and assign all values given.
 *     4)Change all the string value in uppercase.
 *     5)Print the list. 
 */
package collections;


import java.util.Arrays;
import java.util.List;

public class Districts {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("Madurai", "Coimbatore", "Theni", "Chennai", "Karur", "Salem", "Erode", "Trichy" );
        list.replaceAll(String::toUpperCase);
        System.out.println(list);
    }

}