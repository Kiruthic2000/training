/*
 * Requirement:
 *     Demonstrate linked hash set to array() method in java
 *     
 * Entity:
 * 	   LinkedHashSet
 * 
 * Function:
 *     public static void main(String[] args)
 *     
 * Jobs to be Done:
 *     1)Create the package collections.
 *     2)Import LinkedHashSet
 *     3)Create the class LinkedHashSetDemo.
 *     4)Create reference for LinkedHashSet ,that named as set.
 *     5)Add the value to the set.
 *     6)convert to array by using toArray function.
 *     7)Print the array by using for each loop.
 */



package collections;
import java.util.LinkedHashSet;

public class LinkedHashSetDemo {
	
	public static void main(String[] args) {
		LinkedHashSet<String> set = new LinkedHashSet<>();
		set.add("Balaji");
		set.add("Boobalan");
		set.add("Gokul");
		set.add("Karthi");
		set.add("Kiruthic");
		System.out.println("The Linked Hash Set " + set);
		Object[] arr = set.toArray();
		System.out.println("The array is");
		for(Object name : arr) {
			System.out.println(name + " ");
		}
		
	}

}