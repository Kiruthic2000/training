/*
 * Requirement:
 *      demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() )
 * Entity:
 *      MapInterface
 * Function declaration:
 *      public static void main(String[] args) 
 * Jobs to be done:
 *      1)Create package named as collections and import TreeMap.
 *      2)Create class MapInterface and define main method in it.
 *      3)Create reference for TreeMap and name it as map.
 *      4)Perform put(),remove(),replace(),containsValue() method in it.
 */
package collections;

import java.util.TreeMap;

public class MapInterface {

    public static void main(String[] args) {
        TreeMap<Integer, String> map = new TreeMap<>();
        map.put(1, "Karthi");
        map.put(2, "Gokul");
        map.put(3, "Boobalan");
        map.put(4, "Kiruthic");
        map.put(5, "Balaji");
        System.out.println("The map elements are " + map);
        map.remove(5);
        System.out.println("After removing element the map is " + map);
        System.out.println(map.containsValue("Balaji"));
        map.replace(4, "balaji");
        System.out.println("After replacing the key and value of map " + map);
    }
}