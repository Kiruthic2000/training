/*
 * Requirement:
 *     Use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),
 *     pollFirst(),poolLast() methods to store and retrieve elements in ArrayDequeue .
 *     
 * Entity:
 *     ArrayDequeDemo
 *     
 * Function:
 *     public static void main(String[] args) 
 *     
 * Jobs to be Done;
 *     1)Create the package collections.
 *     2)Import the ArrayDeque.
 *     3)Create the class ArrayDequeDemo.
 *     4)Create the reference for ArrayDeque as deque.
 *     5)Add the elements to the list.
 *     6)Add the first element  " 123" to the list and then print the list.
 *     7)Add the last element to the list and then print list.
 *     8)Removing the first and last element in the list , then print list.
 *     9)print the first and last peek element by using "peekFirst" and "peekLast" function.
 *     10)print the first and last poll element by using "pollFirst" and "pollLast" function.
 */


package collections;
import java.util.ArrayDeque;

public class ArrayDequeDemo {
	
	public static void main(String[] args) {
		ArrayDeque<Integer> deque = new ArrayDeque<>();
		deque.add(1);
		deque.add(2);
		deque.add(3);
		deque.add(4);
		deque.add(5);
		deque.add(6);
		deque.add(7);
		deque.add(8);
		System.out.println("The ArrayDeque " + deque);
		deque.addFirst(123);
		System.out.println("After adding the first element" + deque);
		deque.addLast(987);
		System.out.println("After adding the last element" + deque);
		deque.removeFirst();
		System.out.println("After removing the first element" + deque);
		deque.removeLast();
		System.out.println("After removing the first element" + deque);
		System.out.println("The first peek element " + deque.peekFirst());
		System.out.println("The last  peek element " + deque.peekLast());
		System.out.println("The first poll element " + deque.pollFirst());
		System.out.println("The last poll element " + deque.pollLast());
		
	}

}