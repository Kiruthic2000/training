/*
 * Requirements:
 *    LIST CONTAINS 10 STUDENT NAMES
      krishnan, abishek, arun,vignesh, kiruthiga, murugan,adhithya,balaji,vicky, priya and 
      display only names starting with 'A'.
    
 * Entities:
 *    StudentNames
 *    
 * Function Declaration:
 *    public static void main(String[] args)
 *    
 * Jobs To Be Done:
 *    1.Create the package collections.
 *    2.import List, Arrays.
 *    3.Create the class StudentNames.
 *    4.Create the object list as Generic type and the string values are converted into list.
 *    5.Inside the list the values are converted into uppercase.
 *    6.Using for each loop and if condition the names are startswith "A" prints the name.
 */
package collections;

import java.util.List;
import java.util.Arrays;


public class StudentNames {

	public static void main(String[] args) {
		List<String> list = Arrays.asList("krishnan", "abishek", "arun", "vignesh", "kiruthiga",
				"murugan", "adhithya", "balaji", "vicky", "priya");
		System.out.println("list" + list);
		list.replaceAll(String::toUpperCase);
		System.out.println("Uppercase of the list is" + list);
		for (String names : list) {
			if (names.startsWith("A")) {
				System.out.println(names);
			}
		}
	}
}