/*
 * Requirements : 
 * 		To do the following:
 * Create a set
 *  => Add 10 values
 *  => Perform addAll() and removeAll() to the set.
 *  => Iterate the set using 
 *      - Iterator
 *      - for-each
 *
 * Entities :
 * 		public class SetDemo.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 	    1)Create a HashSet.
 *          1.1)Declare the object name as set.
 *          1.2)Add the elements to the set of type Integer.
 *      2)1)Create a another HashSet.
 *          2.1)Declare the object name as newSet.
 *          2.2)Add all the elements to the newSet of type Integer.
 *      3)Remove all the elements in the set and print the set.
 *      4)create a Iterator and declare the object as iterator.
 *          4.1)print all the elements using the Iterator.
 *      5)Print the elements using for each loop.
 *  
 * Pseudo code:
 * public class SetDemo {

    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        //Add elements to the set
        HashSet<Integer> newSet = new HashSet<>();
        newSet.addAll(set);
        System.out.println("After using addAll method : " + newSet);
        //Add elements to the set
        newSet.removeAll(set);
        System.out.println("After using removeAll method : " + newSet);

        // using Iterator
        System.out.println("Using Iterator");
        Iterator<Integer> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();

        // using for each
        System.out.println("Using for-each");
        for (int values : set) {
            System.out.print(values + " ");
        }
    }

}

 * 		
 */
package com.kpr.training.list_and_set;

import java.util.HashSet;
import java.util.Iterator;

public class SetDemo {

	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>();
		set.add(1000);
		set.add(2000);
		set.add(3000);
		set.add(4000);
		set.add(5000);
		set.add(6000);
		set.add(7000);
		set.add(8000);
		set.add(9000);
		set.add(100000);

		HashSet<Integer> newSet = new HashSet<>();
		newSet.addAll(set);
		System.out.println("After using addAll method : " + newSet);
		newSet.add(100);
		newSet.add(200);
		newSet.removeAll(set);
		System.out.println("After using removeAll method : " + newSet);

		// using Iterator
		System.out.println("Using Iterator");
		Iterator<Integer> iterator = set.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();

		// using for each
		System.out.println("Using for-each");
		for (int values : set) {
			System.out.print(values + " ");
		}
	}

}