/*
 * Requirements : 
 * 		To explain the working of contains(), isEmpty() and give example.
 * Entities :
 * 		public class SetExample.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)Create a HashSet with Integer type.
 *          1.1)Add the elements to the set of type Integer.
 *      2)Check if the set has a specific element.
 *          2.1)If it has a specific element then print "true" .
 *          2.2)If it does not have a specific element then print "false" .
 *      3) Check if the set is empty or not.
 *          3.1)If it is empty Print "true" .
 *          3.2)If it is not empty Print"false" .
 *          
 * Pseudo code:
 * public class SetExample {

    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        //Add elements to the set
        System.out.println(set.contains(element)); // returns true
        System.out.println(set.isEmpty()); // returns false
    }

}
 * Explanation:
 * 	contains(<T>):
 * 		returns true if the element is present in the list else returns false.
 * 	isEmpty(<T>):
 * 		returns true if the set is empty else returns false.
 */
package com.kpr.training.list_and_set;

import java.util.HashSet;

public class SetExample {

    public static void main(String[] args) {
        HashSet<Integer> set = new HashSet<>();
        set.add(12);
        set.add(30);
        set.add(500);
        System.out.println(set.contains(500)); // returns true
        System.out.println(set.isEmpty()); // returns false
    }

}