/*Requirement:
 *      write a program for Java String Regex Methods?
 *      
 *Entity:
 *      RegexMethods
 *      
 *Function Signature:
 *      public static void main(String[] args)
 *      
 *Jobs to be Done:
 *      1)Create a variable text of type String and assign the input .
 *      2)Declare the pattern.
 *          2.1)Find if the pattern maches the text and store the result in boolean.
 *          2.2)print the matches.
 *      3)Split the text using split function.
 *      4)Print the array using Iteration.
 *      
 *      
 *Pseudo code:
 *public class RegexMethods {

    public static void main(String[] args) {

        //Get the sentence
        String text =
                "Regular expressions are used for defining String patterns that can be used for searching, manipulating and editing a text";

        String pattern = ".can.";
        boolean matches = Pattern.matches(pattern, text);

        System.out.println("matches = " + matches);

        String[] array = text.split("are");
        for (String string : array) {
            System.out.println(string);
        }


    }
  }

 *
 * 
 */



package com.kpr.training.regex;

import java.util.regex.Pattern;

public class RegexMethods {

    public static void main(String[] args) {

        String text =
                "Regular expressions are used for defining String patterns that can be used for searching, manipulating and editing a text.";

        String pattern = ".can.";
        boolean matches = Pattern.matches(pattern, text);

        System.out.println("matches = " + matches);

        String[] array = text.split("are");
        for (String string : array) {
            System.out.println(string);
        }


    }
}