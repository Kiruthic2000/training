/*
 * Requirement:
 *      To print minimal person with name and email address from the Person class using java.util.Stream<T> API by referring Person.java
 * 
 * Entity:
 *      MinimalPersonMap
 *
 * Function Signature:
 *      public static <R, T> void main(String[] args)
 *
 * Jobs To Be Done:
 *      1) Create the List with reference to precreated method in person.java file.
 *      2) Create two arraylist with reference to string for storing name and mailId
 *      3) Get the name and mailId from precreated method and store it in array.
 *      4) Find min name and mail Id and print it.
 * Pseudocode:
 * 
 * public class MinimalPerson {
    
    public static <R, T> void main(String[] args) {
        List<Person> roster = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> mailId = new ArrayList<>(); 
        for (Person p : roster) {
               //get name 
               //get mailId
        }
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }

}
        
 */
package com.kpr.training.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MinimalPerson {
    
    public static <R, T> void main(String[] args) {
        List<Person> roster = Person.createRoster();
        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> mailId = new ArrayList<>();
        for (Person p : roster) {
            name.add(p.getName());
            mailId.add(p.getEmailAddress());
        }
        String minimalName = Collections.min(name);
        String minimalId = Collections.min(mailId);

        System.out.println(
                "The Minimal Person Name is " + minimalName + " And EmailId is " + minimalId);
    }

}