/*
 * Requirement:
 * 		To Can the field and method called privately using reflection?
  		if so name the function and execute with an example code.
  		
  	Entity:
  		PrivateFieldMethod
  	
  	Method Signature:
  		public static void main(String[] args) 
  		
  	Jobs to be Done:
  		1)Create a object with parameterized constructor for PrivateFieldMethod.
  		2)Access the Field and method to the variable privateField and privateMethod respectively.
  		3)
 */

package com.kpr.training.reflections;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class PrivateFieldMethod {
	
	private String name;
	private int age;
 
 
	public String getName() {
		return name;
	}
 
	public void setName(String name) {
		this.name = name;
	}
 
	private int getAge() {
		return age;
	}
 
	public void setAge(int age) {
		this.age = age;
	}
 
	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + "]";
	}
 
	public PrivateFieldMethod(String name, int age) {
		super();
	}
 
	
	public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException {
		try {
			PrivateFieldMethod detail = new PrivateFieldMethod("John",30);
			
			Field privateField = PrivateFieldMethod.class.getDeclaredField("name");
			Method privateMethod = PrivateFieldMethod.class.getDeclaredMethod("getAge");
			
			privateField.setAccessible(true);
			privateMethod.setAccessible(true);
			
			String name = (String) privateField.get(detail);
			System.out.println("Name of Employee: " + name);
			
			int age = (int) privateMethod.invoke(detail);
			System.out.println("Age of Employee: " + age);
			
		} catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchFieldException e) {
			e.printStackTrace();
		}
	}
}