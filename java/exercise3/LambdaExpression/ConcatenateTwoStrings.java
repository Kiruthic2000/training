/*
 * Requirements : 
 * 		 write a Lambda expression program with a single method interface
 *   To concatenate two strings
 * Entities :
 * 		interface MyInterface,
 * 		public classConcatenateTwoStrings.
 * Function Declaration :
 * 		String concat(String one, String two),
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Importing the FunctionalInterface.
 * 		2)Creating a MyInterface interface and create a concat method.
 * 		3)Creating main method.
 * 		3)Creating test reference and defining a lambda function.
 * 		4)Printing the result using concat method.
 */
package LambdaExpression;

import java.lang.FunctionalInterface;

@FunctionalInterface
interface MyInterface{

	String concat(String one, String two);
}
public class ConcatenateTwoStrings {

	public static void main(String[] args) {

		String one = "Kiruthic";
		String two = " P";
		MyInterface test = (str, str1) -> str + str1;
		System.out.println(test.concat(one, two));
	}

}