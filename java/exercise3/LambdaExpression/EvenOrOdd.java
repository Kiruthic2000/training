/*
 * Requirements : 
 * 		 To convert the following anonymous class into lambda expression.
 *  interface CheckNumber {
	    public boolean isEven(int value);
	}
	
	public class EvenOrOdd {
	    public static void main(String[] args) {
	        CheckNumber number = new CheckNumber() {
	            public boolean isEven(int value) {
	                if (value % 2 == 0) {
	                    return true;
	                } else return false;
	            }
	        };
	        System.out.println(number.isEven(11));
	    }
	}
 * Entities :
 * 		interface CheckNumber,
 * 		public class EvenOrOdd.
 * Function Declaration :
 * 		boolean isEven(int number);
 * 		public static void main(String[] args).
 * Jobs To Be Done:
 * 		1)Importing the FunctionalInterface and Scanner.
 * 		2)Creating a CheckNumber interface and create a isEven method.
 * 		3)Creating a EvenOrOdd class.
 * 		4)Creating main method.
 * 		5)Creating a scanner object and a variable num and getting a input.
 * 		5)Creating check reference and defining a lambda function.
 * 		6)Printing the result using isEven method.
 */
package LambdaExpression;

import java.lang.FunctionalInterface;
import java.util.Scanner;

@FunctionalInterface
interface CheckNumber {
	
	boolean isEven(int number);
}

public class EvenOrOdd {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		int num = scanner.nextInt();
		CheckNumber check = (int number) -> (number % 2 == 0) ? true : false;
		System.out.println(check.isEven(num));

	}

}