/*
 * Requirements : 
 * 		To do the following:
 * Create a set
 *  => Add 10 values
 *  => Perform addAll() and removeAll() to the set.
 *  => Iterate the set using 
 *      - Iterator
 *      - for-each
 *
 * Entities :
 * 		public class SetDemo.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)Importing the HashSet, Iterator.
 * 		2)Creating the SetDemo class
 * 		3)Creating the set reference as generic type.
 * 		4)Adding the set values.
 * 		5)Creating newSet reference as generic type.
 * 		6)Adding elements to newSet using addAll method.
 * 		7)Printing the newSet.
 * 		8)Removing the elements using removeAll method.
 * 		9)Printing the newSet.
 * 		10)Printing the newSet using Iterator and for-each.
 * 		
 */
package listAndSet;

import java.util.HashSet;
import java.util.Iterator;

public class SetDemo {

	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>();
		set.add(1000);
		set.add(2000);
		set.add(3000);
		set.add(4000);
		set.add(5000);
		set.add(6000);
		set.add(7000);
		set.add(8000);
		set.add(9000);
		set.add(100000);

		HashSet<Integer> newSet = new HashSet<>();
		newSet.addAll(set);
		System.out.println("After using addAll method : " + newSet);
		newSet.add(100);
		newSet.add(200);
		newSet.removeAll(set);
		System.out.println("After using removeAll method : " + newSet);

		// using Iterator
		System.out.println("Using Iterator");
		Iterator<Integer> iterator = set.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();

		// using for each
		System.out.println("Using for-each");
		for (int values : set) {
			System.out.print(values + " ");
		}
	}

}