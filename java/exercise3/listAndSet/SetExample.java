/*
 * Requirements : 
 * 		To explain the working of contains(), isEmpty() and give example.
 * Entities :
 * 		public class SetExample.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)Importing the HashSet.
 * 		2)Creating main method and creating the list reference.
 * 		3)Adding elements to the set.
 * 		4)explaining contains(),isEmpty() method inside a print statement.
 */
/*
 * Explanation:
 * 	contains(<T>):
 * 		returns true if the element is present in the list else returns false.
 * 	isEmpty(<T>):
 * 		returns true if the set is empty else returns false.
 */
package listAndSet;

import java.util.HashSet;

public class SetExample {

	public static void main(String[] args) {
		HashSet<Integer> set = new HashSet<>();
		set.add(100);
		set.add(300);
		set.add(400);
		System.out.println(set.contains(100)); // returns true
		System.out.println(set.isEmpty()); // returns false
	}

}