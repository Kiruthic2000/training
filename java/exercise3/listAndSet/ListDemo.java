/*
 * Requirements : 
 * 		To do the following:
 * Create a list
 *   => Add 10 values in the list
 *   => Create another list and perform addAll() method with it
 *   => Find the index of some value with indexOf() and lastIndexOf()
 *   => Print the values in the list using 
 *       - For loop
 *      - For Each
 *       - Iterator
 *       - Stream API
 *   => Convert the list to a set
 *   => Convert the list to a array
 *
 * Entities :
 * 		public class ListDemo.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)Importing the ArrayList, Stack, Arrays, HashSet, Iterator, Set, Stream.
 * 		2)Creating the ListDemo class
 * 		3)Creating the list object as generic type.
 * 		4)Adding the list values.
 * 		5)Creating newList reference as generic type.
 * 		6)Adding all elements to newList from list.
 * 		7)Printing the newList using for Loop.
 * 		8)Printing the newList using for each.
 * 		9)Printing the newList using Iterator.
 * 		10)Printing the newList using for Stream.
 * 		11)Converting newList to a set using HashSet constructor.
 * 		12)Converting newList to a array using .toArray() method.
 */
package listAndSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

public class ListDemo {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(4);
		list.add(8);
		list.add(9);
		list.add(4);
		System.out.println(list);

		ArrayList<Integer> newList = new ArrayList<>();
		newList.addAll(list);
		System.out.println("After adding all values to newlist : " + newList);
		System.out.println("Index of a value of 4 is : " + newList.indexOf(4));
		System.out.println("Last index value of 4 is : " + newList.lastIndexOf(4));

		// using for loop
		System.out.println("Using for loop :");
		for (int index = 0; index < newList.size(); index++) {
			System.out.print(newList.get(index) + " ");
		}
		System.out.println();

		// using For Each
		System.out.println("Using for each :");
		for (int value : newList) {
			System.out.print(value + " ");
		}
		System.out.println();

		// Using iterator
		System.out.println("using iterator :");
		Iterator<Integer> iterator = newList.iterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + " ");
		}
		System.out.println();

		// Using stream api
		System.out.println("Using Stream :");
		Stream<Integer> stream = newList.stream();
		stream.forEach(value -> System.out.print(value + " "));
		System.out.println();

		// converting list to a set
		Set<Integer> set = new HashSet<>(newList);
		System.out.println("Converted to Set : " + set);

		// converting to a array
		Integer[] array = new Integer[newList.size()];
		array = newList.toArray(array);
		System.out.println("converted to array : " + Arrays.toString(array));
	}

}