/*
 * Requirements : 
 * 		To explain about contains(), subList(), retainAll() and give example
 * Entities :
 * 		public class ListExample.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)Importing the ArrayList
 * 		2)Creating main method and creating the list reference.
 * 		3)Adding elements to the list.
 * 		4)explaining contains(),sublist() method inside a print statemeny.
 * 		5)Creating newList reference and adding some elements.
 * 		6)explaining retainAll() method.
 */
/*
 * Explanation:
 * 	contains():
 * 		returns true if the element is present in the list else returns false.
 * 	subList(starting index, ending index):
 * 		returns the list elements from starting index to ending index.
 * 	retainAll():
 * 		It is used to remove all the array list's elements that are not contained in the specified list.
 */
package listAndSet;

import java.util.ArrayList;

public class ListExample {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(100);
		list.add(200);
		list.add(300);
		list.add(400);
		list.add(500);
		list.add(600);
		System.out.println(list.contains(500)); // it prints true
		System.out.println(list.subList(1, 4)); //prints elements from index 1 to 3
		
		ArrayList<Integer> newList = new ArrayList<>();
		newList.add(400);
		newList.add(300);
		newList.add(1000);
		System.out.println("Before using retainall method : " + newList);
		newList.retainAll(list);
		System.out.println("After using retainall method : " + newList);
	}

}