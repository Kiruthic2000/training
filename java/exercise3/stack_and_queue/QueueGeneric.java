/*
 * Requirement:
 *   Create a stack using generic type and implement
    -> Push atleast 5 elements
    -> Pop the peek element
    -> search a element in stack and print index value
    -> print the size of stack
    -> print the elements using Stream
  Entity:
      QueueGeneric
  Function Declaration:
       public static void main(String[] args)
   Jobs to be done:
      1)Creating the package stack_and_queue.
      2)Importing the LinkedList,Queue ,PriorityQueue and Stream package.
      3)Creating the QueueGeneric class
      4)Creating the queue and queue1 object with queue class as generic type using LinkedList and PriorityQueue.
      5)Pushing the 5 elements and the value of elements are 1,2,3,4 and 5.
      6)Printing the queue elements,removed element,after removing front element ,boolean value if queue contains element,
        Size of the queue and the queue elements using stream.
      
 */
package stack_and_queue;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.Stream;

public class QueueGeneric {

    public static void main(String[] args) {
    	System.out.println("Using LinkedList");
        Queue<Integer> queue = new LinkedList<>();
        queue.add(1);
        queue.add(2);
        queue.add(3);
        queue.add(4);
        queue.add(5);
        System.out.println("The queue elements are ");
        System.out.println(queue);
        // Removing the front element
        System.out.println("Removing the front element " + queue.remove());
        System.out.println("After removing the first element " + queue);
        // Check if Queue Contains Element
        boolean element = queue.contains(3);
        System.out.println(element);
        System.out.println("The size of the queue is " + queue.size());
        Stream<Integer> stream = queue.stream();
        System.out.println("The queue element are printed by using stream");
        stream.forEach(elements -> System.out.print(elements + " "));
        
        System.out.println("Using Priority Queue");
        Queue<Integer> queue1 = new PriorityQueue<>();
        queue1.add(1);
        queue1.add(2);
        queue1.add(3);
        queue1.add(4);
        queue1.add(5);
        System.out.println("The queue elements are ");
        System.out.println(queue1);
        // Removing the front element
        System.out.println("Removing the front element " + queue1.remove());
        System.out.println("After removing the first element " + queue1);
        // Check if Queue Contains Element
        boolean element1 = queue1.contains(3);
        System.out.println(element1);
        System.out.println("The size of the queue is " + queue1.size());
        Stream<Integer> stream1 = queue1.stream();
        System.out.println("The queue element are printed by using stream");
        stream1.forEach(elements -> System.out.print(elements + " "));
        
    }

}