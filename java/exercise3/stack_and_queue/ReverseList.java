/*
 * Requirements : 
 * 		Reverse List Using Stack with minimum 7 elements in list.
 * Entities :
 * 		public class ReverseList.
 * Function Declaration :
 * 		public static void main(String[] args)
 * Jobs To Be Done:
 * 		1)In stack_and_queue package, importing the arraylist and stack.
 * 		2)Creating the ReverseList class
 * 		3)Creating the list object as generic type.
 * 		4)Appending the list values.
 * 		5)Creating the stack object in generic type.
 * 		6)Pushing each element of the list to the stack.
 * 		7)Clearing the list elements to append the reversed list.
 * 		8)Using for loop,adding the popped element from the stack.
 * 		9)printing the reversed Stack list
 */
package stack_and_queue;

import java.util.ArrayList;
import java.util.Stack;

public class ReverseList {
	
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(4);
		list.add(3);
		list.add(2);
		list.add(1);
		list.add(0);
		list.add(-1);
		System.out.println("Before Reversing : " + list);
		
		Stack<Integer> stack = new Stack<>();
		for(Integer value : list) {
			stack.push(value);
		}
		list.clear();
		int size = stack.size();
		for(int iteration = 0; iteration < size; iteration++) {
			list.add(stack.pop());
		}
		System.out.println("After Reversing : " + list);
		
	}
}