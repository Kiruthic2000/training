/*
 * Requirement: To create a stack using generic type and implement 1)Push atleast 5 elements. 2)Pop
 * the peek element. 3)Search a element in stack and print the index value. 4)print the size of
 * stack. 5)print the elements using stream. Entity: StackGeneric
 * 
 * Function Declaration: No function is declared
 * 
 * Jobs To Be Done: 1)Creating the package stack_and_queue. 2)Importing the Stack and Stream
 * package. 3)Creating the StackGeneric class 4)Creating the test object with stack class as generic
 * type. 5)Pushing the 5 elements and the value of elements are 12,5,8,3 and 37. 6)Printing the
 * Peek,poped element of the stack, Stack after poped, Index of the element, Size of the Stack and
 * the stack elements using stream.
 */
package stack_and_queue;

import java.util.Stack;
import java.util.stream.Stream;

public class StackGeneric {
	
	public static void main(String[] args) {
		
		Stack<Integer> test = new Stack<>();
		test.push(12);
		test.push(5);
		test.push(8);
		test.push(03);
		test.push(37);
		System.out.println("The peek element is " + test.peek());
		System.out.println("The poped element is " + test.pop());
		System.out.println("The Stack after poped is " + test);
		System.out.println("The Index of the element '8' is " + test.indexOf(8));
		System.out.println("The Size of the Stack is " + test.size());
		
		Stream<Integer> stream = test.stream();
		System.out.print("The Stack elements printed using Stream ");
		stream.forEach(i -> System.out.print(i + " "));
	}


}
