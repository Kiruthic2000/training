/*
 * Requirements:
 *    1) To write a Java program to copy all of the mappings from the specified map to another map?
 *    2) To write a Java program to test if a map contains a mapping for the specified key?
 *    3) Count the size of mappings in a map?
 *    4) To Write a Java program to get the portion of a map whose keys range from a given key to another key?
 
 * Entities:
 *    MapDemo
 
 * Function Declaration:
 *    public static void main(String[] args) 
 
 * Jobs To Be Done:
 *    1.Create the package map.
 *    2.Import the TreeMap.
 *    3.Create the class MapDemo and also create the object map as generic type.
 *    4.Put the keys and the values are in map, prints the map.
 *    5.create the another object map1 as Generic type.
 *    6.map values are copy to map1 Using putAll() method.
 *    7.Print the output of map1, size of the map1 and get the values using get(key) method and particular ranges between the keys,
 *    values using subMap().      
 */
package map;

import java.util.TreeMap;

public class MapDemo {

	public static void main(String[] args) {
		TreeMap<Integer, String> map = new TreeMap<>();
		map.put(1, "EEE");
		map.put(2, "ECE");
		map.put(3, "MECH");
		map.put(4, "BME");
		map.put(5, "CHEMICAL");
		System.out.println(map);

		TreeMap<Integer, String> map1 = new TreeMap<>();
		map1.putAll(map);
		System.out.println(map1);
		System.out.println(map1.size());
		System.out.println(map1.get(3));
		System.out.println(map1.subMap(2, 5));

	}
}
