/* Requirement: 
        Can we write only try block without catch and finally blocks?why?
 * Entity:
 *       TryWithoutCatch
 * Function declaration: 
 *      public static void main(String[] args)
 * Jobs to be done:  
 *     Find whether try block will execute without catch and finally block or not.
 * 
 * Solution:
 *      In a a program try block should be accompanied by either catch block or finally block or both.
 * A program without catch or finally block will throw syntax error as the exception will not get handled.
*/
 package exceptionHandling;

 public class TryWithoutCatch {

 public static void main(String[] args) {
        
        try{
           System.out.println(2/0);
        }
        //Exception in thread "main" java.lang.Error: Unresolved compilation problem: 
        //Syntax error, insert "Finally" to complete BlockStatements

    }
 }