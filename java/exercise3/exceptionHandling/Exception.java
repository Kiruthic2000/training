/*
 * Requirement:
 *     Handle and give the reason for the exception in the following code: 
 *     
 * Entity:
 * 	   Exception
 * 
 * Function:
 *     public static void main(String[] args)
 * 
 * Jobs to be Done:
 *     1)Creating the package exception_handling.
 *     2)Creating the class Exception
 *     3)Assigning the values to arr of type int.
 *     4)Print the value at the index of 7.
 *     
 *     
 * Answer:
 *     The array index is out of range ,so it through a Exception "ArrayIndexOutOfBoundsException"
 *     
	
 */

package exceptionHandling;

public class Exception {  
	
    public static void main(String[] args) {
    	
    try
    {
    	
    	int arr[] ={1,2,3,4,5};
    	System.out.println(arr[7]);
    	
    } catch (ArrayIndexOutOfBoundsException e) {
    	System.out.println(" The array index out of Bounds Exception occurs");
    }
 }
}