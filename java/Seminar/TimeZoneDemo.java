import java.util.*;

public class TimeZoneDemo {

    public static void main(String[] args) {

        Calendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
        calendar.set(Calendar.HOUR_OF_DAY, 12);

        System.out.println("UTC: " + calendar.get(Calendar.HOUR_OF_DAY)); //UTC: 12
        System.out.println("UTC: " + calendar.getTimeInMillis());         //UTC: 1598618902435

        calendar.setTimeZone(TimeZone.getTimeZone("Europe/Copenhagen")); 
        System.out.println("CPH: " + calendar.get(Calendar.HOUR_OF_DAY));  //CPH: 14
        System.out.println("CPH: " + calendar.getTimeInMillis());          //CPH: 1598618902435

        calendar.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        System.out.println("NYC: " + calendar.get(Calendar.HOUR_OF_DAY));  //NYC: 8
        System.out.println("NYC: " + calendar.getTimeInMillis());          //NYC: 1598618902435

    }
}

