import java.util.*; // It is accessed through java.util.Locale
import java.text.*;

public class Locale {
    public static void main(String[] args) {
		Locale locale = new Locale ("en","IN");  //English Language, INDIA
		Locale locale1 = Locale.GERMAN;
		Locale locale2 = new Locale.Builder().setLanguage("da").setRegion("DK").build();
        int n = 123456789;
        NumberFormat numberFormat = NumberFormat.getInstance(locale2);
        String number = numberFormat(n);
		System.out.println(locale);  //en_IN
		System.out.println(locale1); //de
		System.out.println(locale2); //da_DK
        System.out.println(number);
	}
}