import java.util.Locale;
import java.text.NumberFormat;

public class LocaleDemo {

    public static void main(String[] args) {

        Locale locale = new Locale ("en","IN");  //English Language, INDIA
        Locale locale1 = Locale.GERMAN;
        Locale locale2 = new Locale.Builder().setLanguage("da").setRegion("DK").build();

        int n = 1234567890;
        NumberFormat numberFormat = NumberFormat.getInstance(locale);
        String number = numberFormat.format(n);

        System.out.println(locale);  //en_IN
        System.out.println(locale1); //de
        System.out.println(locale2); //da_DK
        System.out.println(number); //123,456,789

    }
}

