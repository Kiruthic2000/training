Several application has to work on several timezone. For that TimeZone is used.

UTC
    UTC is short for Coordinated Universal Time (Universal Time Coordinated ). UTC is the time in the UK time zone (like Greenwich Mean Time).

Time Zones
    All time zones are calculated as offsets to UTC time. For instance, the time zone in Copenhagen, Denmark is UTC + 1 meaning UTC time plus one hour. 

Daylight Savings
    UTC time is independent of daylight savings time. That means, that in the summer time, the time in Copenhagen, Denmark is UTC + 2, meaning UTC time plus two hours.

To Store Date and Time:
    It first convert date and time according to UTC time.

To Convert between time zone: java.util.Calender is imported.
