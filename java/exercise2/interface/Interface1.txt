/*
Requirement:
	To show methods would a class that implements the
	java.lang.CharSequence interface have to implement

Entity:
	No entity

Functions Declared:
	No Functions.

Jobs To Be Done:
	1) Answering the above question.

*/
Solution:

1) charAt()- returns the character at specified Index.
2) length() - return the total length of the string.
3) subSequence(int start, int end) - Returns a subsequence of this sequence.
4) 	toString() - Returns a string containing the characters in this sequence in the same order as this sequence.