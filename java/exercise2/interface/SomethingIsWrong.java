/*Requirement:
    To identify what is wrong in the given interface program and also fixing it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }

Entity:
    SomethingIsWrong is the Entity.

Function Declaration:
    void aMethod(int aValue) is the function declared.

Jobs to be done:
    1. Analysing the given expression.
    2. It has a method implementation in it.Only default and static variables can implements can have a method implementations.
	3. So the corrected code has a static or default type method in it.
*/
public interface SomethingIsWrong {
    static void aMethod(int aValue){     // Here default or static can be used.
        System.out.println("Hi Mom");