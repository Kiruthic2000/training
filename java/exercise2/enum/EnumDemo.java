/*
Requirement:
    To compare the enum values using equals method and == operator.

Entity:
    public class EnumDemo.

Function Declaration:
    public static void main(String[] args).

Jobs to be Done:
    1. First create a class called EnumDemo.
    2. Now a enum type day is created and the values are stored.
    3. After the type day is created another type month is created and the values are stored.
    4. Now there are two types of comparision of enum values using euals method and == operator.
    5. Firsly the variable or object is created for for the first type as day and the value is given
       as null.
    6. When compared using == operator it gives the result false while when equals method is used it
       throws a null pointer exception due to equals method cannot call the null method.
    7. When the value for day is given as Day.Monday there is no exception thrown.
    8.Secondly when twon enum types are used == operator checks for the same type if it fails it
      throws an error or in other words == operator is type compatability where as equals method
      gives an output
    9. When we print Month.January == Day.Monday it throws an error but when using equals method it 
       shows the output as false.
*/

//Answer:

public class EnumDemo {

    public enum Day {
        Monday,
        Tuesday,
        Wednesday,
        Thrusday,
        Friday,
        Saturday,
        Sunday
    }

    public enum Month {
        January,
        Feburary,
        March,
        April,
        June,
        July,
        August,
        September
    }

    public static void main(String[] args) {
        Day day = null;
        System.out.println(day == Day.Monday);                // prints false
        // shows null pointer exception because null cannot be called by a method.
        System.out.println(day.equals(Day.Monday));
        System.out.println(Month.January == Day.Monday);      // shows error
        System.out.println(Month.January.equals(Day.Monday)); // prints false
        Month month = Month.April;
        System.out.println(month == Month.April);             // prints true
        System.out.println(month.equals(Month.April));        // prints true
    }
}