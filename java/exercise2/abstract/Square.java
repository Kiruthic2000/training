import com.shape.training.core.Shape;

public class Square extends Shape {
    public double side;
    
    public Square(double side) {
        this.side = side;
    }
    
    public void printArea() {
        System.out.println(" Square Area is:" + side * side);
        
    }
    public void printPerimeter() {
        System.out.println(" Square Perimeter is:" + 4*side);
    }
}