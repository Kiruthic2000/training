import com.shape.training.core.Shape;
import java.util.Scanner;
public class AbstractDemo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double side = scanner.nextDouble();
        Square square = new Square(side);
        System.out.println("side of the square:" + side);
        square.printArea();
        square.printPerimeter();
        double radius = scanner.nextDouble();
        Circle circle = new Circle(radius);
        System.out.println("radius of the circle:" + radius);
        circle.printArea();
        circle.printPerimeter();
    }
}