import com.shape.training.core.Shape;

public class Circle extends Shape {
    public double radius;
    
    public Circle(double radius) {
        this.radius = radius;
    }
    public void printArea() {
        System.out.println("Circle Area:" + 3.14*radius*radius);
    }
    public void printPerimeter() {
        System.out.println("Circle Perimeter:" + 2*3.14*radius);
    }
    
}