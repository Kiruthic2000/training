/*
Requirement:
	To find the operator which is used to invert the value of a boolean.

Entity:
	Operator

Function Declaration:
	No Function Declared.

Jobs To Be Done:
	1)Declaraing the class name as Operator. 
	2)Declared the boolean variables as true and false.
	3)Printing the inverse of operator by '!'

*/
public class Operator {
	public static void main(String[] args) {
		boolean operator = true;
		boolean operator1 = false;
		System.out.println("!Operator = " + !operator);  //prints false
		System.out.println("!Operator1 = " + !operator1);  //prints true
	}
}
		