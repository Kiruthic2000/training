/*
Requirement:
    To demonstrate overloading with Wrapper types.

Entity:
    WrapperOverLoading

Function declaration:
    test(Integer),test(Float),test(Double),test(Long)
    public static void main(String[] args)

Jobs to be done:
    1.Create a class WrapperOverLoading.
    2.Declare the methods as test with different wrapper types.
    3.Create an object for the class and pointer as value.
    4.Call the methods with different data type values like integer,float,double,long.
    5.Print the respective values to wrapper types.
*/

//PROGRAM:
public class WrapperOverLoading {

    public void test(Integer value) {
        System.out.println("Integer : " + value);
    }
    public void test(Float value) {
        System.out.println("Float : " + value);
    }
    public void test(Double value) {
        System.out.println("Double : " + value);
    }
    public void test(Long value) {
        System.out.println("Long : " + value);
    }
    public static void main(String[] args) {
        WrapperOverLoading value = new WrapperOverLoading();
        value.test(45);
        value.test(9.32f);
        value.test(145.7532d);
        value.test(1254693L);
    }
}