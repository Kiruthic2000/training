/*
Requirement:
    To print the classname of all the primitive data types.

Entity:
    PrimitiveClassNames
    
Function Declaration:
    public static void main(String[] args)
    getName()
    
Jobs To Be Done:
    1.Create the class PrimitiveClassNames.
    2.Print the respective classname of datatypes by using getName() method. 
*/
public class PrimitiveClassNames {
    public static void main(String[] args) {
        String intClassName = int.class.getName();
            System.out.println("Class Name of Int : " + intClassName);
        
        String charClassName = char.class.getName();
            System.out.println("Class Name of Char : " + charClassName);
        
        String doubleClassName = double.class.getName();
            System.out.println("Class Name of double : " + doubleClassName);
        
        String floatClassName = float.class.getName();
            System.out.println("Class Name of float : " + floatClassName);
    }
}