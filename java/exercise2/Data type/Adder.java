/*
Requirement:
	To read the Input from command line and add it together.

Entity:
	Adder.

Function Declaration:
	public void add(int... array)

Jobs To Be Done:
	1)Class Adder is declared.
	2)Input is got from the command line.
	3)Calling the function add.
	4)Checking whether the length of array is not equal to four, if false the elements in the array is added.
	5)the sum of individual elements is printed.

*/
import java.util.Scanner;	
public class Adder {

    public void add(int... array) {
        if(array.length != 4) {
            System.out.println("Add more numbers");
        } else {
            int sum = 0;
            for(int number : array) {
                sum += number;
            }
            System.out.println(sum);
        }
    }
    public static void main(String[] args) {
	Scanner obj = new Scanner(System.in);
    int adder = obj.nextInt(); 
    adder.add();
    }
}